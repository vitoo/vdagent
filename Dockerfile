FROM fedora:latest 

RUN dnf install -y git libtool make ImageMagick  \
      wine-core.x86_64 wine-core.i686 msitools \
    #   mingw32-libpng-static mingw32-zlib-static mingw32-gcc-c++ mingw32-winpthreads-static  \
      mingw64-libpng-static mingw64-zlib-static mingw64-gcc-c++ mingw64-winpthreads-static


RUN mkdir -p /root/vdagent
 
CMD git config --global --add safe.directory /root/vdagent && \
   autoreconf -if && mingw64-configure && \
   mingw64-make && \
   mingw64-make msi

WORKDIR /root/vdagent